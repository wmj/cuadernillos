#!/usr/bin/env python

import argparse


def getparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("template", type=argparse.FileType("r"))
    parser.add_argument("style", type=argparse.FileType("r"))
    parser.add_argument("scripts", type=argparse.FileType("r"))
    return parser


def main(args):
    print(args.template.read().format(
        STYLE=args.style.read(),
        SCRIPTS=args.scripts.read()
    ))


if __name__ == "__main__":
    parser = getparser()
    main(parser.parse_args())
